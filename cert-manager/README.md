# Sécurité avant tout

Nous allons gérer les certificats avec [cert-manager](https://cert-manager.io/)

## Installation

`cert-manager` fournit un Helm chart, configurons le repo:

```bash
helm repo add jetstack https://charts.jetstack.io
helm repo update
```

Et maintenant nous pouvons installer `cert-manager` dans notre cluster dans un namespace dédié

```bash
helm install \
  cert-manager jetstack/cert-manager \
  --namespace cert-manager \
  --create-namespace \
  --version v1.13.3 \
  --set installCRDs=true
```

!!! success "cert-manager est installé"
    ```console
    cert-manager v1.13.3 has been deployed successfully!
    ```

Maintenant, vérifions que tout est ok:

```bash
kubectl get all -n cert-manager
```

```console
NAME                                         READY   STATUS    RESTARTS   AGE
pod/cert-manager-6856dc897b-k6dks            1/1     Running   0          32s
pod/cert-manager-cainjector-c86f8699-cmc7t   1/1     Running   0          32s
pod/cert-manager-webhook-f8f64cb85-7rjjz     1/1     Running   0          32s

NAME                           TYPE        CLUSTER-IP     EXTERNAL-IP   PORT(S)    AGE
service/cert-manager           ClusterIP   10.3.242.74    <none>        9402/TCP   33s
service/cert-manager-webhook   ClusterIP   10.3.182.243   <none>        443/TCP    33s

NAME                                      READY   UP-TO-DATE   AVAILABLE   AGE
deployment.apps/cert-manager              1/1     1            1           33s
deployment.apps/cert-manager-cainjector   1/1     1            1           33s
deployment.apps/cert-manager-webhook      1/1     1            1           33s

NAME                                               DESIRED   CURRENT   READY   AGE
replicaset.apps/cert-manager-6856dc897b            1         1         1       33s
replicaset.apps/cert-manager-cainjector-c86f8699   1         1         1       33s
replicaset.apps/cert-manager-webhook-f8f64cb85     1         1         1       33s
```

## Configuration

`cert-manager` permet de générer des certificats pour sécuriser nos endpoints. Il supporte plusieurs providers, ici nous allons utiliser le classique mais pratique [Let's Encrypt](https://letsencrypt.org/fr/).

Pour cela, nous devons créer un `Issuer`, ici un `ClusterIssuer` pour accéder à l'ensemble du cluster

```yaml
apiVersion: cert-manager.io/v1
kind: ClusterIssuer
metadata:
  name: letsencrypt-production
  namespace: cert-manager
spec:
  acme:
    server: https://acme-v02.api.letsencrypt.org/directory
    email: <your-email> # (1)
    privateKeySecretRef:
      name: letsencrypt-production # (2)
    solvers:
      - http01:
          ingress:
            class: nginx # (3)
```

1. 📧 Un email est nécessaire pour l'appartenance du domaine
2. 📜 On utilise le serveur de production
3. 🛜 On précise que l'on répondra au challenge HTTP en utilisant un  `Ingress` de type `nginx`

Créons notre issuer:

```bash
kubectl apply -n cert-manager -f cert-manager/letsencrypt-cluster-issuer.yml
```

!!! success "Issuer créé"
    ```console
    clusterissuer.cert-manager.io/letsencrypt-production created
    ```

??? info "Utilisation du serveur de staging"
    Par défaut, on utilise le serveur de production de Let's Encrypt, mais celui à la limitation que si vous avez un nombre de requêtes en échec trop importants, vous pouvez être banni temporairement.
    
    Pour une phase de test, il est possible d'utiliser le serveur de staging de Let's Encrypt qui n'a pas cette limitation.

    ```yaml
    apiVersion: cert-manager.io/v1
    kind: ClusterIssuer
    metadata:
      name: letsencrypt-staging
      namespace: cert-manager
    spec:
      acme:
        server: https://acme-staging-v02.api.letsencrypt.org/directory
        email: <your-email>
        privateKeySecretRef:
          name: letsencrypt-staging
        solvers:
          - http01:
              ingress:
                class: nginx
    ```

## Utilisation

Maintenant, on peut sécuriser nos URLs. Il suffit d'updater notre `Ingress` pour lui configurer le TLS:


```yaml
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: secured-deployment-ingress
  annotations:
    cert-manager.io/cluster-issuer: letsencrypt-production
spec:
# [...]
  tls:
    - hosts:
        - secured.<votre-trigramme>.grunty.uk
      secretName: secured.<votre-trigramme>.grunty.uk-tls
```

Déployons notre nouvel ingress

```bash
kubectl apply -f demos/deployment-with-ingress-https-grunty.yml
```

```console
ingress.networking.k8s.io/secured-deployment-ingress created
```

On peut voir que `cert-manager` instancie un `Ingress` pour gérer les échanges avec Let's Encrypt pour la génération du certificat:

```bash
kubectl get ingress -n demos
```

```console
NAME                         CLASS    HOSTS                                       ADDRESS         PORTS     AGE
cm-acme-http-solver-hmh8t    <none>   secured.<votre_trigramme>.grunty.uk                         80        47s
new-deployment-ingress       nginx    new-deployment.<votre_trigramme>.grunty.uk  57.128.120.31   80        19h
secured-deployment-ingress   nginx    secured.<votre_trigramme>.grunty.uk                         80, 443   50s
```

On voit aussi qu'un `Certificate` a été généré. Il est au statut `Ready` à `False`, au bout de quelques secondes, il devrait passer à `True``

```bash
kubectl get certificates -n demos
```

!!! success "Certificat généré"
    ```
    NAME                                      READY   SECRET                                    AGE
    secured.<votre_trigramme>.grunty.uk-tls   True    secured.<votre_trigramme>.grunty.uk-tls   3m44s
    ```

??? example "Behind the scene"
    On peut facilement voir les étapes en observant les events

    ```bash
    kubectl get events --sort-by='.lastTimestamp' -n demos
    ```

Tout semble ok, on peut vérifier que notre URL est désormais sécurisée : [secured.\<votre_trigramme\>.grunty.uk](https://secured.<votre_trigramme>.grunty.uk)

!!! success "Site accessible en HTTPs"

Mais notre chef de brigade est vraiment pointilleux 😅, notre gestion des données sensibles tels que nos API keys ne lui convient pas.
Alors pimentons un peu tout cela avec une gestion de secrets plus propre [➡️](../external-secrets/README.md)

