# IaC tu connait ?

Ok , on réussi notre petite recette. Tout est écrit mais ça serait encore mieux si on pouvait automatiser tout ça.

Pour cela on va utiliser Flux.

## Pré-requis

Pour boostraper Flux, il faut que la personne qui lance la commande ait les droits cluster admin sur le cluster Kubernetes cible. Il est aussi nécessaire que la personne qui lance la commande soit le propriétaire du projet GitLab, ou ait les droits admin d'un groupe GitLab.
N'oubliez pas de [forker le projet](../recipe.md#cest-parti) pour pouvoir le modifier. ET de basculer sur votre nouveau repo ;-)

!!! info Pas besoin de forker 2 fois
    Si vous avez fait la partie [environnements gitlab](../gitlab/README.md) avant celle-ci et que vous avez déjà forké le projet, pas besoin de faire un nouveau fork.
    Vous pouvez réutiliser le 1er.

Ensuite, il va nous falloir un token GitLab pour que Flux puisse se connecter à notre repo GitLab.

### GitLab PAT (Personal Access Token)

On va récupérer le token et le stocker dans une variable d'environnement.

```bash
export GITLAB_TOKEN=<THE TOKEN>
```

??? tip "Comment récupérer un token GitLab"
    Pour créer un token GitLab, il faut aller dans votre profil GitLab, puis dans `Preferences > Access Tokens`
    ![Profile](PAT1.png)


    ![Create PAT](PAT2.png)


    Et créer un token avec les bons scopes:
    ![PAT Scopes](PAT3.png)

## Installation de Flux

La premiére étape c'est d'installer Flux Cli.

```bash
curl -s https://fluxcd.io/install.sh | sudo bash
```

## Cette fois c'est la bonne on configure Flux

On va demander à la CLI flux de s'installer sur notre cluster et de se connecter à notre repo GitLab.

Tout est décrit [dans le tuto](https://fluxcd.io/flux/installation/bootstrap/gitlab/) Flux.

!!! info
    Si la variable d'environement GITLAB_TOKEN n'est pas renseignée, le boostrap va demander de saisir le token.
    
    Il est possible de fournir le token avec une commande du type: `echo "<gl-token>" | flux bootstrap gitlab`.

On lance le bootstrap sur le projet avec notre compte personnel:
```bash
flux bootstrap gitlab \
  --deploy-token-auth \
  --owner=<NAMESAPCE_NAME> \
  --repository=<PROJECT_STUG> \
  --branch=main \
  --path=flux/awsome-cluster/ \
  --personal
```

Si le projet n'existe pas, Flux va le créer pour vous en privé. Si vous souhaitez créer un projet public, définissez `--private=false`.

Lorsque l'on utilise `--deploy-token-auth`, la CLI génére un token GL et le stock dans le cluster sous la forme d'une Secret qui s'appel **flux-system** dans le Namespace **flux-system**.

??? tip "Flux bootstrap output"
    ```console 
    ► connecting to https://gitlab.com
    ► cloning branch "main" from Git repository "https://gitlab.com/yodamad-workshops/kub-workshop.git"
    ✔ cloned repository
    ► generating component manifests
    ✔ generated component manifests
    ✔ committed sync manifests to "main" ("4271d8d7adef5572f1031f0f21767d449d0ccbb4")
    ► pushing component manifests to "https://gitlab.com/yodamad-workshops/kub-workshop.git"
    ► installing components in "flux-system" namespace
    ✔ installed components
    ✔ reconciled components
    ► checking to reconcile deploy token for source secret
    ✔ configured deploy token "flux-system-main-flux-system-./flux/awsome-cluster" for "https://gitlab.com/yodamad-workshops/kub-workshop"
    ► determining if source secret "flux-system/flux-system" exists
    ► generating source secret
    ► applying source secret "flux-system/flux-system"
    ✔ reconciled source secret
    ► generating sync manifests
    ✔ generated sync manifests
    ✔ committed sync manifests to "main" ("05dfd597d5959fda3a783f2336b65d1f1d7b121d")
    ► pushing sync manifests to "https://gitlab.com/yodamad-workshops/kub-workshop.git"
    ► applying sync manifests
    ✔ reconciled sync configuration
    ◎ waiting for Kustomization "flux-system/flux-system" to be reconciled
    ✔ Kustomization reconciled successfully
    ► confirming components are healthy
    ✔ helm-controller: deployment ready
    ✔ kustomize-controller: deployment ready
    ✔ notification-controller: deployment ready
    ✔ source-controller: deployment ready
    ✔ all components are healthy
    ```
 Bien joué !

L'agent Flux va maintenant surveiller notre repo GitLab et appliquer les changements.

## Petite visite de notre nouvelle cuisine

### Le cellier
Si on pull le repo GitLab, on peut voir que Flux à créer un nouveau repertoire `flux/awsome-cluster/flux-system` qui contient la configuration de Flux :

* `kustomization.yaml` ce fichier est un index, on va lister ici les manifests qui doivent être pris en compte dans ce répertoire.
* `gotk-components.yaml` ce fichier contient la définition des RBAC et des CRDs (Custom Resource Definition) utilisée par Flux.
* `gotk-sync.yaml` définit la manières dont l'opérateur se connecte au repo au travers du **Kind** `GitRepository` et le type **Kustomization** permet de configurer quel sont les manifest / configuration à scupter. Pour nous tout ce qui se trouve dans `./flux/awsome-cluster` est utilisé comme configuration

Ici le fichier `./flux/awsome-cluster/flux-system/gotk-sync.yaml` contient la configuration de Flux pour se connecter à notre repo GitLab.

```yaml title="./flux/awsome-cluster/flux-system/gotk-sync.yaml" linenums="1" hl_lines="5 15"
apiVersion: source.toolkit.fluxcd.io/v1
kind: GitRepository
metadata:
#ICI on trouve le nom de notre Objet GitRepository
  name: flux-system
  namespace: flux-system
spec:
  interval: 1m0s
  ref:
# ICI la branche à utiliser
    branch: main
  secretRef:
    name: flux-system
# ICI vous retrouverez l'adresse de votre repo GitLab    
  url: https://gitlab.com/jouve.thomas/kub-workshop-snowcamp-2024.git
```

### Notre première recette

On va pouvoir lui dire d'appliquer la configuration grace aux objets **Kustomization**.

Si on regarde par example le fichier `flux/repo.yaml`
```yaml title="./flux/repo.yaml" linenums="1" hl_lines="8 12"
--8<-- "flux/repo.yaml"
```

On peux voir que l'on décrit en language **Flux** un nouveau répertoire à surveiller `./flux/repository` dans notre `GitRepository:flux-system`.

#### Définition des HelmRepository
Dans ce répertoire `./flux/repository` on retrouve par example le fichier `nginx.yaml` :
```yaml title="./flux/repository/nginx.yaml" linenums="1" hl_lines="4"
--8<-- "flux/repository/helm/nginx.yaml"
```

Cette fois ci on configure un **HelmRepository** qui va permettre à **Flux** de récupérer les informations sur les charts disponibles dans le repo **Helm**.
On pourra faire référence à ce chart sous le nom `nginx-ingress-controller`.

### Nouveau commis

On va maintenant deplacer ce fichier `repo.yaml` dans le répertoire `flux/awsome-cluster/` qui est le seul, pour le moment, que connait **Flux**.

Effectivement le fichier `gotk-sync.yaml` indique que seul le répertoire `./flux/awsome-cluster` est scrupté par **Flux** :

```yaml title="./flux/awsome-cluster/flux-system/gotk-sync.yaml" linenums="1" hl_lines="8"
apiVersion: kustomize.toolkit.fluxcd.io/v1
kind: Kustomization
metadata:
  name: flux-system
  namespace: flux-system
spec:
  interval: 10m0s
  path: ./flux/awsome-cluster
  prune: true
  sourceRef:
    kind: GitRepository
    name: flux-system
```

## On envoye les commandes en cuisine

Et on `push commit`, car maintenant c'est **Flux** qui se charge de faire la synchronisation sur le cluster depuis notre repo.

```bash 
cp flux/repo.yaml flux/awsome-cluster/repo.yaml
git add flux/awsome-cluster/repo.yaml
git commit -am ":satellite_orbital: Setup Helm repos" && git push
```

On peux observer la réconciliation avec la commande suivante :

```bash
flux get kustomizations --watch
```

On a quelque chose comme ça :

* il y a 2 répertoires à surveiller (2 ```Kustomization```)
* la synchronisation est active (```SUSPENDED : False```)
* et à jour (```READY : True```)

Il indique aussi quelle est la révision utilisée pour la synchronisation (ici :```main@sha1:c80d7d4c```).

```console
NAME            REVISION                SUSPENDED       READY   MESSAGE                              
flux-system     main@sha1:c80d7d4c      False           True    Applied revision: main@sha1:c80d7d4c
repos   main@sha1:c80d7d4c      False   True    Applied revision: main@sha1:c80d7d4c
```

On peux vérifier en regardant si il à bien créer nos resources ```HelmRepository``` :
  
  ```bash 
  kubectl get HelmRepository -A
  ```
  
  ![Alt text](HelmRepository-A.png)

### Ingress Controller

Tout est pret dans le répertoire `./nginx-ingress-controller/flux` pour déployer notre Ingress Controller.

#### Le descriptif de notre recette une ```Kustomization```
!!! note "Ici que ce n'est pas une ```Kustomization``` mais une ```Kustomization``` 🧌"

Il faut [lire](https://fluxcd.io/flux/faq/#are-there-two-kustomization-types) :

* *Kustomization*@**kustomize.toolkit.fluxcd.io/v1** : C'est la [CRD de FLux](https://fluxcd.io/flux/components/kustomize/kustomizations/) pour les objects Flux (le liens vers un repo / repertoire / interval de scrapping)
* *Kustomization*@**kustomize.config.k8s.io/v1beta1** : c'est l'objet [Kustomize](https://kubectl.docs.kubernetes.io/references/kustomize/) de Kubernetes ()

Ici on déclare une recette avec le nom `flux-nginx-ingress-controller` et qu'il est nécessaire d'utiliser les fichiers `nginx-ingress-controller.yaml` et `ns.yaml` pour déployer notre Ingress Controller.

```yaml title="./nginx-ingress-controller/flux/ks.yaml" linenums="1" hl_lines="4 6 7"
--8<-- "nginx-ingress-controller/flux/ks.yaml"
```


#### La definition de notre Namespace
Avec un simple fichier manifest vanilla :

```yaml title="./nginx-ingress-controller/flux/ns.yaml" linenums="1"
--8<-- "nginx-ingress-controller/flux/ns.yaml"
```

#### Et la definition de notre HelmRelease
```yaml title="./nginx-ingress-controller/flux/nginx-ingress-controller.yml" linenums="1" hl_lines="10 14 17 21 22 23"
--8<-- "nginx-ingress-controller/flux/nginx-ingress-controller.yml"
```

#### Chaud devant !

Avant de lancer la commande en cuisine, soyons fou et supprimont notre Ingress Controller précédement installé pour laisser faire **Flux**.

!!! note  "Ce n'est pas obligatoire, mais c'est pour voir la magie de Flux."
    En fait flux va juste réappliquer la configuration, donc si vous ne supprimez pas l'Ingress Controller, il va juste le mettre à jour.
    Il utilisera ```install``` ou ```upgrade``` en fonction de l'état de l'objet HelmRelease.

```bash	
 helm list -A
 helm uninstall ingress-nginx -n nginx-ingress-controller
 helm list -A
 kubectl get all -n nginx-ingress-controller
 kubectl delete ns nginx-ingress-controller
 kubectl get ns
```

#### On envoye la sauce

On va maintenant deplacer le fichier `flux/nginx-ingress-controller.yaml` dans le répertoire `flux/awsome-cluster/` comme pour les repos Helm.

```yaml title="./flux/nginx-ingress-controller.yaml" linenums="1" hl_lines="8"
--8<-- "flux/nginx-ingress-controller.yaml"
```

```bash 
cp flux/nginx-ingress-controller.yaml flux/awsome-cluster/nginx-ingress-controller.yaml
git add flux/awsome-cluster/nginx-ingress-controller.yaml
git commit -am ":satellite_orbital: Setup Ingress Controller" && git push
```
On observe la syncrho avec la commande suivante :

```bash
flux get kustomizations --watch
```

```bash	
 helm list -A
 echo "Helm list ne nous retourne rien car c'est Flux qui gére maintenant"
 echo "On utilise : "
 kubectl get HelmRepository -A
 kubectl get HelmChart -A
 kubectl get HelmRelease -A
 kubectl get ns
 kubectl get all -n nginx-ingress-controller
```