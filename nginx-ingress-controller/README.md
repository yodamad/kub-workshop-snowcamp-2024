# Controller nos Ingress

Pour nous aider à gérer nos Ingress, [ingress-nginx-controller](https://github.com/kubernetes/ingress-nginx) va nous aider pour automatiquement faire le routage depuis l'IP externe.

D'abord, il faut ajouter le repo Helm pour le chart:

```bash
helm repo add ingress-nginx https://kubernetes.github.io/ingress-nginx
helm repo update
```

Ensuite, on peut installer le chart: (1)
{ .annotate }

1. ℹ️ l'option `create-namespace` force la création d'un nouveau namespace

```bash
helm upgrade --install ingress-nginx ingress-nginx/ingress-nginx \
  --namespace nginx-ingress-controller \
  --create-namespace \
  --set controller.publishService.enabled=true
```

On peut vérifier que tout est installé correctement

```bash
helm ls -A
```

!!! success annotate "Helm installé"
    ```console
    NAME         	NAMESPACE               	REVISION	UPDATED                             	STATUS  	CHART              	APP VERSION
    ingress-nginx	nginx-ingress-controller	1       	2023-12-19 15:33:53.505518 +0100 CET	deployed	ingress-nginx-4.8.4	1.9.4       # (1)
    ```

1. La version `1.9.4` peut variée

On peut vérifier que le namespace et les composants sont bien créés :

```bash
kubectl get ns
```

!!! success "Le namespace est créé"
    ```console
    NAME                       STATUS   AGE
    default                    Active   5h17m
    kube-node-lease            Active   5h17m
    kube-public                Active   5h17m
    kube-system                Active   5h17m
    nginx-ingress-controller   Active   10s
    ```

```bash
kubectl get all -n nginx-ingress-controller
```

!!! success "Les composants sont instanciés et opérationnels"
    ```console
    NAME                                            READY   STATUS    RESTARTS   AGE
    pod/ingress-nginx-controller-75967d99c9-9vcwr   1/1     Running   0          2m45s

    NAME                                         TYPE           CLUSTER-IP    EXTERNAL-IP     PORT(S)                      AGE
    service/ingress-nginx-controller             LoadBalancer   10.3.28.249   57.128.120.49   80:31253/TCP,443:31582/TCP   2m46s
    service/ingress-nginx-controller-admission   ClusterIP      10.3.10.52    <none>          443/TCP                      2m46s

    NAME                                       READY   UP-TO-DATE   AVAILABLE   AGE
    deployment.apps/ingress-nginx-controller   1/1     1            1           2m46s

    NAME                                                  DESIRED   CURRENT   READY   AGE
    replicaset.apps/ingress-nginx-controller-75967d99c9   1         1         1       2m46s
    ```

!!! info
    Cela peut prendre un peu de temps avant que l'IP externe soit disponible.


Une fois l'IP externe disponible, on peut facilement la récupérer:

```bash
export EXT_IP=$(kubectl get service ingress-nginx-controller -n nginx-ingress-controller -o jsonpath='{.status.loadBalancer.ingress[0].ip}')
echo ${EXT_IP:-NOT_SET_WAIT_AND_RETRY}
```

Désormais, notre `ingress-controller` va automatiquement géré le routage pour nous lors de l'ajout d'`Ingress` dans le cluster.

Pour cela, il faut ajouter un `Ingress` à notre déploiement précisant l'URL sur laquelle exposé le service:

```yaml
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: simple-deployment-ingress
  namespace: demos
  annotations:
    external-dns.alpha.kubernetes.io/cloudflare-proxied: "false" # (1)
spec:
  ingressClassName: nginx
  rules:
    - host:  new-deployment.<votre_trigramme>.grunty.uk # (2)
      http:
        paths:
          - backend:
              service:
                name: new-deployment-service
                port:
                  number: 80
            pathType: Prefix
            path: /
---
```

1. ℹ️ Spécificité lorsque l'on utilise cloudflare
2. 🛜 URL sur laquelle on veut exposer le service

!!! tip
    Pensez bien à modifier le `yaml` avec votre trigramme avant de faire le `apply`

```bash
kubectl apply -f demos/deployment-with-ingress-grunty.yml
export my_host=new-deployment.<votre_trigramme>.grunty.uk
echo "Site URL http://"${my_host}
```

!!! success "Demo déployée"
    Le pod est démarré :
    ```yaml
    kubectl get po -n demos
    ```
    donne
    ```console
    NAME                                READY   STATUS    RESTARTS   AGE
    new-deployment-5998d8dbcc-kdbrk     1/1     Running   0          9m21s
    simple-deployment-5897799cb-94xzv   1/1     Running   0          58m
    ```

    et l'ingress créé avec la bonne url:
    ```bash
    kubectl get ingress -n demos
    ```
    donne
    ```console
    NAME                     CLASS   HOSTS                          ADDRESS         PORTS   AGE
    new-deployment-ingress   nginx   new-deployment.mvt.grunty.uk   57.128.120.31   80      9m32s
    ```

Grâce à l'ingress controller, nous n'avons plus besoin de faire le `port-forward` comme précédemment car le routage est fait sur l'URL.
On peut donc tester d'accéder à nos pods via `curl`

2 méthodes possibles:

En forcant un header `Host` avec l'URL voulue:

```bash
echo "Host: ${my_host}" "http://${EXT_IP}/"
curl --header "Host: ${my_host}" "http://${EXT_IP}/"
```
ou, en surchargeant la résolution DNS dynamiquement:

```bash
curl --resolve ${my_host}:80:${EXT_IP} -H "Host: ${my_host}" -i "http://${EXT_IP}"
```

Les 2 commandes donnent le même résultat: (1)
{ .annotate }

1. ⏳ La commande avec l'option `--resolve` est légèrement plus verbeuse

```console
Server address: 10.2.1.6:80
Server name: new-deployment-5998d8dbcc-kdbrk
Date: 19/Dec/2023:15:07:19 +0000
URI: /
Request ID: 4246cc4f7d0d56a49792cc73024dc779
```

Ok ca fonctionne, mais ca reste pas super pratique à utiliser 🫤

💡 Ca serait pratique, si automatiquement nos entrées DNS étaient crées et publiées pour que ce soit accessible depuis un navigateur.

Et si `external-dns` était la solution ⁉️

🧂 Alors ajoutons-en une pincée pour voir [➡️](../external-dns/README.md)
